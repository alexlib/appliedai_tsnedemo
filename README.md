# Applied AI TSNE Demo (high dim dataviz)

_Applied AI Internal Demo Project_


Demo of high dimensional data visualisation with t-distributed Stochastic Neighbor Embedding (t-SNE) using the [Urban Land Cover Classification](https://archive.ics.uci.edu/ml/datasets/Urban+Land+Cover) dataset from the UCI Machine Learning Repository.



## Development:

### Git clone the repo to your workspace.

e.g. in Mac OSX terminal:

        workspace$ git clone https://{username}@bitbucket.org/appliedai/appliedai_tsnedemo.git
        workspace$ cd appliedai_tsnedemo



### Setup a virtual environment for Python libraries

**NOTE:** using Python 3.4

Using Anaconda distro,

1. create a new virtual environment, installing packages from requirements file:


        conda create -n appliedai_tsnedemo python=3.4 --file requirements_conda_mac
        source activate appliedai_tsnedemo



Then manually:

1. OPTIONAL AS OF SCIKIT-LEARN 0.17, WHICH INCLUDES A BH-TSNE IMPLEMENTATION
   install package `tsne` (for the faster Barnes-Hut implementation) either:

    a. also from pypi `pip install tsne`, OR

    b. if like Jon you have Mac OSX 10.10 and the cblas is not where the setup.py expects it to be....

        i. run `locate cblas.h` and copy the main path for OSX 10.10 SDK
        ii. clone, fork and locally store the repo `https://github.com/danielfrg/tsne`
        iii. modify line 18 in file `setup.py` accordingly e.g.:
            extra_compile_args=['-I/Applications/Xcode.app/ \
            Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/ \
            MacOSX10.10.sdk/System/Library/Frameworks/Accelerate.framework/ \
            Versions/A/Frameworks/vecLib.framework/Versions/A/Headers']
        iv. pip install from the forked repo e.g.
            pip install git+file:///Users/Jon/ExternalRepos/tsne/

---



## Data:

Directly download the [zipfile](https://archive.ics.uci.edu/ml/machine-learning-databases/00295/Urban%20land%20cover.zip) to the /data folder

    $ cd data
    $ wget https://archive.ics.uci.edu/ml/machine-learning-databases/00295/Urban%20land%20cover.zip
    $ unzip Urban\ land\ cover.zip


---

## Presentation:


### Live presentation using RISE

For live presentation of file `02_Presentation.ipynb`, we're using RISE, available at https://github.com/damianavila/RISE

1. Clone the repo
2. Source activate your python environment (if not already done)
3. Run `python setup.py install` from within the RISE directory, this will install the nbextension
4. Run `ipython notebook` as normal, and click the newly added button "Enter/Exit Live Reveal Slideshow"


Consider customization:

+ Set a decent theme and transition in

        /Users/me/.jupyter/nbconfig/livereveal.json

edit `https://github.com/damianavila/RISE/blob/master/src/less/main.less`

and rebuild the css as instructed here: https://github.com/damianavila/RISE#development


+ For system-wide styling, consider configuring:

        /Users/me/.ipython/profile_default/static/custom/custom.css


### Static presentation using plain old reveal.js


+ At the terminal, use nbconvert:

        $> ipython nbconvert 02_Presentation.ipynb --to slides

This creates `02_Presentation.html` which you can serve using e.g.

        $> python -m http.server

